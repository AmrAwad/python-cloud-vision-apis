import logging


def setup_logger(name):
    formatter = logging.Formatter(fmt='%(asctime)s - %(module)s - %(message)s',
                                  datefmt='%H:%M:%S')

    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.addHandler(handler)
    return logger
