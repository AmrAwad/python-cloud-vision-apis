from sqlalchemy import Column, ForeignKey, Integer, String, Float
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Image(Base):
    __tablename__ = 'image'
    id = Column(Integer, primary_key=True)
    url = Column(String(250), nullable=False)
    name = Column(String(50), nullable=False)


class Face(Base):
    __tablename__ = 'face'
    id = Column(Integer, primary_key=True)
    confidence = Column(Float, nullable=False)
    width = Column(Float, nullable=False)
    height = Column(Float, nullable=False)
    top = Column(Float, nullable=False)
    left = Column(Float, nullable=False)
    image_id = Column(Integer, ForeignKey('image.id'))
    image = relationship('Image', backref=backref('faces'))


class Emotion(Base):
    __tablename__ = 'emotion'
    id = Column(Integer, primary_key=True)
    name = Column(String(20), nullable=False)
    confidence = Column(Float, nullable=False)
    face_id = Column(Integer, ForeignKey('face.id'))
    face = relationship('Face', backref=backref('emotions', order_by='Emotion.confidence'))


class Object(Base):
    __tablename__ = 'object'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=False)
    confidence = Column(Float, nullable=False)
    image_id = Column(Integer, ForeignKey('image.id'))
    image = relationship('Image', backref=backref('objects'))
