import os
import boto3
from .utils import download_image_from_url
from .models import Image, Face, Emotion, Object
from configparser import ConfigParser
from .log import setup_logger

logger = setup_logger('root')
logger.debug('Root lLogger initialized')


class AmazonClient:
    def __init__(self, ACCESS_KEY, SECRET_KEY, BUCKET_NAME, REGION):
        self.BUCKET_NAME = BUCKET_NAME

        self.client = boto3.client("rekognition",
                                   REGION,
                                   aws_access_key_id=ACCESS_KEY,
                                   aws_secret_access_key=SECRET_KEY)

        self.s3 = boto3.resource('s3',
                                 aws_access_key_id=ACCESS_KEY,
                                 aws_secret_access_key=SECRET_KEY)

        logger.info("Amazon client initialized")

    @classmethod
    def from_config(cls):
        config = ConfigParser()
        filename = os.path.join(os.path.dirname(__file__), 'config.secret')
        logger.info("Loading configurations from %s", filename)
        config.read(filename)
        return cls(
            ACCESS_KEY=config['AWS Credentials'].get('ACCESS_KEY'),
            SECRET_KEY=config['AWS Credentials'].get('SECRET_KEY'),
            BUCKET_NAME=config['AWS S3'].get('BUCKET_NAME'),
            REGION=config['AWS Rekognition'].get('REGION')
        )

    def process_image(self, session, name, url):
        img = Image(name=name, url=url)
        logger.info("Processing: %r", img)
        self.session = session
        self.session.add(img)
        self.session.commit()

        self._s3_upload_image_from_url(url, name)
        self._get_face_details(img)
        self._get_object_details(img)
        logger.info("Image processing completed!")

    def _s3_upload_image_from_disk(self, image_name):
        logger.info("Uploading Image from disk ...")
        self.s3.Bucket(self.BUCKET_NAME).upload_file(image_name, image_name)

    def _s3_upload_image_from_url(self, url, name):
        logger.info("Downloading Image from url ...")
        download_image_from_url(url, name)
        self._s3_upload_image_from_disk(name)

    def _get_object_details(self, img):
        logger.info("Getting Object details ...")
        Image = {'S3Object': {'Bucket': self.BUCKET_NAME, 'Name': img.name}}
        response = self.client.detect_labels(Image=Image)
        self.process_object_data(img, response)

    def _get_face_details(self, img):
        logger.info("Getting face details ...")
        Image = {'S3Object': {'Bucket': self.BUCKET_NAME, 'Name': img.name}}
        response = self.client.detect_faces(Image=Image, Attributes=['ALL'])
        self.process_face_data(img, response)

    def process_object_data(self, img, response):
        logger.info("Processing Object data ...")
        if len(response['Labels']) > 0:
            for label in response['Labels']:
                img.objects.append(Object(name=label['Name'], confidence=label['Confidence']))
                self.session.commit()

    def process_face_data(self, img, response):
        logger.info("Processing face data ...")
        if len(response['FaceDetails']) > 0:
            for face in response['FaceDetails']:
                new_face = Face(confidence=face['Confidence'],
                                left=face['BoundingBox']['Left'],
                                top=face['BoundingBox']['Top'],
                                width=face['BoundingBox']['Width'],
                                height=face['BoundingBox']['Height'])
                img.faces.append(new_face)

                for emotion in face['Emotions']:
                    new_face.emotions.append(Emotion(name=emotion['Type'],
                                                     confidence=emotion['Confidence']))
            self.session.commit()
