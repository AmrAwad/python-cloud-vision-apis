# Amazon Client

This client integrates both AWS S3 & AWS Rekognition to analyse the images.

Current Supported analysis are:
    - Face recognition
    - Object recognition

For setting up the credentials, follow the steps in [this guide](https://boto3.readthedocs.io/en/latest/guide/quickstart.html)

## Configuration File Format

The `AmazonClient` has a method `from_config()` that loads the configurations from a `config.secret` file in the package directory that's not tracked in the source code.

The configuration file has the following format:
```
[AWS Credentials]
ACCESS_KEY = YOUR_ACCESS_KEY
SECRET_KEY = YOUR_SECRET_KEY

[AWS S3]
BUCKET_NAME = S3_BUCKET_NAME

[AWS Rekognition]
REGION = REKOGNITION_REGION
```

## Usage

In addition to a valid `config.secret` You'll need to provide a session in a valid database to store the results:
```python
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from clients import Base

engine = create_engine('sqlite:///test.db')
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()
```

Once the session is created, you can pass it to the AmazonClient along with the url & name of the image to be processed:

```python
from clients import AmazonClient

image_name = "image.jpg"
image_url = "http://api.slv.vic.gov.au/access_record/4737037"


ac = AmazonClient.from_config()
ac.process_image(session=session, url=image_url, name=image_name)
```