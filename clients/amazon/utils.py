import urllib


def download_image_from_url(url, imageName):
    urllib.request.urlretrieve(url, imageName)
