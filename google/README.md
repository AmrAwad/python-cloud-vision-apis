### Installation ###

Only package you need to install is google-cloud-vision
```
$ pip install --upgrade google-cloud-vision
```

### Quickstart ###
* To set up the project, you need to set the environment variable `GOOGLE_APPLICATION_CREDENTIALS` to the path of the json file. 

### Notes ###
When recreating this project you can follow these steps:

* Enable the cloud vision api, follow [this guide](https://cloud.google.com/vision/docs/before-you-begin)
* We will have to create another json file for your project, you can easily do that by following [this guide](https://cloud.google.com/docs/authentication/getting-started)