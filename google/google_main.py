import urllib
import io
import os
# import json

# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types

# Download image


def download_image_from_url(url, imageName):
    urllib.request.urlretrieve(url, imageName)


def get_image_type(imageName):
    with io.open(imageName, 'rb') as image_file:
        content = image_file.read()
    image = types.Image(content=content)
    return image


def get_label_response(imageName):
    image = get_image_type(imageName)
    response = client.label_detection(image=image)
    return response


def get_face_response(imageName):
    image = get_image_type(imageName)
    response = client.face_detection(image=image)
    return response


def get_color_response(imageName):
    image = get_image_type(imageName)
    response = client.image_properties(image=image)
    return response


def process_label_response(response):
    labels = response.label_annotations
    labelDetails = []
    for label in labels:
        details = {
            'description': label.description,
            'score': label.score,
        }
        labelDetails.append(details)
    return labelDetails


def process_face_response(response):
    faces = response.face_annotations
    faceDetails = []
    faceCount = len(response.face_annotations)
    if faceCount > 0:
        for face in faces:
            bounding_box = []
            for point in face.fd_bounding_poly.vertices:
                position = {
                    'x': point.x,
                    'y': point.y,
                }
                bounding_box.append(position)
            likelihood_name = ('UNKNOWN', 'VERY_UNLIKELY', 'UNLIKELY', 'POSSIBLE', 'LIKELY', 'VERY_LIKELY')
            details = {
                'bounding_box': bounding_box,
                'joy_likelihood': likelihood_name[face.joy_likelihood],
                'sorrow_likelihood': likelihood_name[face.sorrow_likelihood],
                'anger_likelihood': likelihood_name[face.anger_likelihood],
                'surprise_likelihood': likelihood_name[face.surprise_likelihood],
                'detection_confidence': face.detection_confidence,
            }
            faceDetails.append(details)
    face_resp = {
        'face_count': faceCount,
        'face_details': faceDetails
    }
    return face_resp


def process_color_response(response):
    colors = response.image_properties_annotation.dominant_colors.colors
    colorDetails = []
    for color in colors[0:3]:
        details = {
            'hex': '#%02x%02x%02x' % (color.color.red, color.color.green, color.color.blue),
            'score': color.score,
        }
        colorDetails.append(details)
    return colorDetails


def reponse_to_file(response, fileName):
    with open(fileName, "w") as fh:
        fh.write(str(response))


def label_main(imageName, label_fileName):
    label_response = get_label_response(imageName)
    # reponse_to_file(label_response, label_fileName)
    labelDetails = process_label_response(label_response)
    return labelDetails


def face_main(imageName, face_fileName):
    face_response = get_face_response(imageName)
    # reponse_to_file(face_response, face_fileName)
    faceDetails = process_face_response(face_response)
    return faceDetails


def color_main(imageName, color_fileName):
    color_response = get_color_response(imageName)
    # reponse_to_file(color_response, color_fileName)
    colorDetails = process_color_response(color_response)
    return colorDetails


def json_response(labelDetails, faceDetails, colorDetails, url):
    import json
    resp_dict = {
        "face_data": faceDetails,
        "label_data": labelDetails,
        "color_data": colorDetails,
        "url": url,
    }
    resp_json = json.dumps(resp_dict)
    return resp_json


def json_to_csv(resp_json, fileName):
    import json
    import csv
    resp_dict = json.loads(resp_json)
    try:
        headers = resp_dict.keys()
    except:
        print("Dictionary is empty")
        return
    with open(fileName, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=headers)
        writer.writeheader()
        writer.writerow(resp_dict)


# The name of the image file to annotate
imageName = "image.jpg"
imageUrl = "http://api.slv.vic.gov.au/access_record/4737037"
label_fileName = "label_response.txt"
face_fileName = "face_response.txt"
color_fileName = "color_response.txt"
csv_filename = "out.csv"

# Instantiates a client
client = vision.ImageAnnotatorClient()

# print("Downloading Image from Url ...")
# download_image_from_url(imageUrl, imageName)
# print("Image download completed!")

print("Getting Label Details ...")
labelDetails = label_main(imageName, label_fileName)

print("Label details acquired! Getting face details ...")
faceDetails = face_main(imageName, face_fileName)

print("Face details acquired! Getting color details ...")
colorDetails = color_main(imageName, color_fileName)

print('Color details acquired! Creating json response ...')
resp_json = json_response(labelDetails, faceDetails, colorDetails, imageUrl)

print('Outputing json response to csv')
json_to_csv(resp_json, csv_filename)
print('All Done :D')
