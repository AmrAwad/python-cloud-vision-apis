import http.client, urllib.request, urllib.parse, urllib.error, base64, requests, json, csv

KEY_FACES = '7da36b061be14421b24a74078c6fe3d8'
KEY_VISION = 'a7a2d60a871e4bb0905c935cd9bb021c'
uri_base = 'https://westcentralus.api.cognitive.microsoft.com'
img_url = 'http://api.slv.vic.gov.au/access_record/4737037'

def process_face_data(response):
    faceDetails = list()
    if response != []:
        for face in response:
            details = {
                'left': face['faceRectangle']['left'],
                'top': face['faceRectangle']['top'],
                'height': face['faceRectangle']['height'],
                'width': face['faceRectangle']['width'],
                'emotion': face['faceAttributes']['emotion'],
            }
            faceDetails.append(details)
    face_resp = {
        'face_count': len(faceDetails),
        'face_details': faceDetails
    }
    return face_resp    

def get_face_data(KEY_FACES, uri_base, img_url):
    face_headers = {
        'Content-Type': 'application/json',
        'Ocp-Apim-Subscription-Key': KEY_FACES,
    }
    face_params = {
        'returnFaceId': 'true',
        'returnFaceLandmarks': 'false',
        'returnFaceAttributes': 'emotion',
    }
    body = {'url': img_url}
    response = requests.request('POST', uri_base + '/face/v1.0/detect', json=body, data=None, headers=face_headers, params=face_params)
    faceDetails = process_face_data(json.loads(response.text))
    return faceDetails

def process_object_data(response):
    objectDetails = list()
    if response['tags']:
        for object in response['tags']:
            details = {
                'Name': object['name'],
                'Confidence': object['confidence'],
            }
            objectDetails.append(details)
    return objectDetails    


def get_object_data(KEY_VISION, uri_base, img_url):
    object_headers = {
        'Content-Type': 'application/json',
        'Ocp-Apim-Subscription-Key': KEY_VISION,
    }
    object_params = {'visualFeatures': 'Tags'}
    body = {'url': img_url}
    response = requests.request('POST', uri_base + '/vision/v1.0/analyze', json=body, data=None, headers=object_headers, params=object_params)
    objectDetails = process_object_data(json.loads(response.text))
    return objectDetails

def process_color_data(response):
    colorDetails = list()
    if response['color']['dominantColors']:
        colorDetails = response['color']['dominantColors']
    return colorDetails

def get_color_data(KEY_VISION, uri_base, img_url):
    color_headers = {
        'Content-Type': 'application/json',
        'Ocp-Apim-Subscription-Key': KEY_VISION,
    }
    color_params = {'visualFeatures': 'Color'}
    body = {'url': img_url}
    response = requests.request('POST', uri_base + '/vision/v1.0/analyze', json=body, data=None, headers=color_headers, params=color_params)
    colorDetails = process_color_data(json.loads(response.text))
    return colorDetails

def json_response(objectDetails, faceDetails, colorDetails, url):
    resp_dict = {
        "face_data": faceDetails,
        "object_data": objectDetails,
        "color_data": colorDetails,
        "url": url,
    }
    resp_json = json.dumps(resp_dict)
    return resp_json

def json_to_csv(resp_json, fileName):
    resp_dict = json.loads(resp_json)
    try:
        headers = resp_dict.keys()
    except:
        print("Dictionary is empty")
        return
    with open(fileName, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=headers)
        writer.writeheader()
        writer.writerow(resp_dict)


if __name__ == "__main__":
    faceDetails = get_face_data(KEY_FACES, uri_base, img_url)
    objectDetails = get_object_data(KEY_VISION, uri_base, img_url)
    colorDetails = get_color_data(KEY_VISION, uri_base, img_url)
    resp_json = json_response(objectDetails, faceDetails, colorDetails, img_url)
    json_to_csv(resp_json, "test.csv")
