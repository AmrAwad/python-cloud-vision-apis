### Installation ###

Only package you need to install is cognitive_face
```
$ pip install cognitive_face
```

### Quickstart ###

For setting up the credentials, you can [request subscription keys](https://docs.microsoft.com/en-us/azure/cognitive-services/Computer-vision/vision-api-how-to-topics/howtosubscribe)

### References ###
* [Face Api Documentation](https://westus.dev.cognitive.microsoft.com/docs/services/563879b61984550e40cbbe8d/operations/563879b61984550f30395236)
