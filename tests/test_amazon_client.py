from clients import AmazonClient, Base, Image, Face, Object
from unittest import TestCase
from unittest.mock import patch, Mock
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import json
import os


class TestAmazonClient(TestCase):
    def _load_responses(self):
        filename = os.path.join(os.path.dirname(__file__), 'amazon_faces.json')
        with open(filename, 'r') as f:
            self.SAMPLE_FACES_RESPONSE = json.load(f)

        filename = os.path.join(os.path.dirname(__file__), 'amazon_objects.json')
        with open(filename, 'r') as f:
            self.SAMPLE_OBJECTS_RESPONSE = json.load(f)

    def _add_patch(self, p):
        p.start()
        self.addCleanup(p.stop)

    def _create_session(self):
        engine = create_engine('sqlite:///:memory:')
        Base.metadata.create_all(engine)
        Session = sessionmaker(bind=engine)
        return Session()

    def setUp(self):
        self._load_responses()

        FakeClient = Mock()
        FakeClient.detect_labels.return_value = self.SAMPLE_OBJECTS_RESPONSE
        FakeClient.detect_faces.return_value = self.SAMPLE_FACES_RESPONSE

        self._add_patch(patch('boto3.resource'))
        self._add_patch(patch('boto3.client', Mock(return_value=FakeClient)))
        self._add_patch(patch('urllib.request'))

        self.amazonClient = AmazonClient("ACCESS_KEY", "SECRET_KEY",
                                         "bucket_name", "rekognition_region")

    def test_process_image(self):
        session = self._create_session()

        image_name = "image.jpg"
        image_url = "http://api.slv.vic.gov.au/access_record/4737037"
        self.amazonClient.process_image(session=session, url=image_url, name=image_name)

        image = session.query(Image).filter_by(name=image_name).one()
        self.assertEquals(image_name, image.name)
        self.assertEquals(image_url, image.url)

        faces = session.query(Face).join(Image).filter(image.name == image_name)
        faces_ref = self.SAMPLE_FACES_RESPONSE['FaceDetails']
        self.assertEquals(len(faces_ref), faces.count())
        self.assertEquals(faces_ref[0]['Confidence'], faces.first().confidence)

        emotions = faces.first().emotions
        self.assertEquals(len(faces_ref[0]['Emotions']), len(emotions))

        objects = image.objects
        objects_ref = self.SAMPLE_OBJECTS_RESPONSE['Labels']
        self.assertEquals(len(objects), len(objects_ref))
        self.assertEquals(objects[0].name, objects_ref[0]['Name'])
