'''
This module is the test runner, it runs tests and produces test and coverage reports.
'''
import unittest
import coverage


def main():
    '''
    Run test suite with coverage.
    '''
    cov = coverage.coverage(branch=True, include='clients/*')
    cov.start()

    test_suite = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(test_suite)

    cov.stop()
    cov.report(show_missing=True)
    cov.html_report(directory='covhtml')


if __name__ == '__main__':
    main()
