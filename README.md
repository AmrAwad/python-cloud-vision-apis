# Cloud Vision API Integration

This project aims to integrate the computer vision APIs from AWS, GCP, Azure, Clarifi. The end result would be the user uploading an image and getting detailed results from all services.

## Architecture Overview
![Architecture Overview](docs/high_level_design.png)