'''
    Author: Amr Awad <amro.khir@gmail.com>
    Date: 27-11-2017

    In order to save the number of trials we need, we can experiment on the response without calling the API
'''


def process_color_response(color_response_dict):
    try:
        numColors = len(color_response_dict['outputs'][0]['data']['colors'])
    except:
        return []

    colorDetails = []
    for index in range(numColors):
        details = {
            "name": color_response_dict['outputs'][0]['data']['colors'][index]['w3c']['name'],
            "hex": color_response_dict['outputs'][0]['data']['colors'][index]['w3c']['hex'],
            "raw_hex": color_response_dict['outputs'][0]['data']['colors'][index]['raw_hex'],
            "value": color_response_dict['outputs'][0]['data']['colors'][index]['value']
        }
        colorDetails.append(details)
    return colorDetails


def load_color_response_from_file(fileName):
    with open(fileName, "r") as f:
        dict = eval(f.read())
    colorDetails = process_color_response(dict)
    return colorDetails


def process_face_response(face_response_dict):
    try:
        numFaces = len(face_response_dict['outputs'][0]['data']['regions'])
    except:
        return []
    faceDetails = []
    for index in range(numFaces):
        details = {
            "id": face_response_dict['outputs'][0]['data']['regions'][index]['id'],
            "box_top_row": face_response_dict['outputs'][0]['data']['regions'][index]['region_info']['bounding_box']['top_row'],
            "box_left_col": face_response_dict['outputs'][0]['data']['regions'][index]['region_info']['bounding_box']['left_col'],
            "box_right_col": face_response_dict['outputs'][0]['data']['regions'][index]['region_info']['bounding_box']['right_col'],
            "box_bottom_row": face_response_dict['outputs'][0]['data']['regions'][index]['region_info']['bounding_box']['bottom_row'],
        }
        faceDetails.append(details)
    return faceDetails


def load_face_response_from_file(fileName):
    with open(fileName, "r") as f:
        dict = eval(f.read())
    faceDetails = process_face_response(dict)
    return faceDetails


def process_object_response(object_response_dict):
    try:
        numObjects = len(object_response_dict['outputs'][0]['data']['concepts'])
    except:
        return []

    objectDetails = []
    for index in range(numObjects):
        details = {
            "id": object_response_dict['outputs'][0]['data']['concepts'][index]['id'],
            "app_id": object_response_dict['outputs'][0]['data']['concepts'][index]['app_id'],
            "name": object_response_dict['outputs'][0]['data']['concepts'][index]['name'],
            "value": object_response_dict['outputs'][0]['data']['concepts'][index]['value'],
        }
        objectDetails.append(details)
    return objectDetails


def load_object_response_from_file(fileName):
    with open(fileName, "r") as f:
        dict = eval(f.read())
    objectDetails = process_object_response(dict)
    return objectDetails


def dict_to_csv_file(resp_dict, fileName):
    import csv
    try:
        headers = resp_dict[0].keys()
    except:
        print("List is empty")
        return

    with open(fileName, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=headers)
        writer.writeheader()
        writer.writerows([item for item in resp_dict])


def json_response(colorDetails, faceDetails, objectDetails, url):
    import json
    resp_dict = {
        "color_data": colorDetails,
        "face_data": faceDetails,
        "object_data": objectDetails,
        "url": url,
    }
    resp_json = json.dumps(resp_dict)
    return resp_json


def json_response_from_file():
    import json
    resp_dict = {
        "color_data": load_color_response_from_file("color_response.json"),
        "face_data": load_face_response_from_file("face_response.json"),
        "object_data": load_object_response_from_file("object_response.json"),
    }
    resp_json = json.dumps(resp_dict)
    return resp_json


def json_to_csv(resp_json, fileName):
    import json
    import csv
    resp_dict = json.loads(resp_json)
    try:
        headers = resp_dict.keys()
    except:
        print("Dictionary is empty")
        return
    with open(fileName, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=headers)
        writer.writeheader()
        writer.writerow(resp_dict)


if __name__ == '__main__':
    '''
    # Parse Color Model response
    colorDetails = load_color_response_from_file("color_response.json")
    print("Color Details:\n------------------")
    print(colorDetails)
    print()

    # Parse Face Model response
    faceDetails = load_face_response_from_file("face_response.json")
    print("Face Details:\n------------------")
    print(faceDetails)
    print()

    # Parse Object Model response
    objectDetails = load_object_response_from_file("object_response_3.json")
    print("Object Details:\n------------------")
    print(objectDetails)
    print()

    # Change response to csv format (Also available with any response)
    faceDetails = load_face_response_from_file("face_response_3.json")
    dict_to_csv_file(faceDetails, "test.csv")

    '''
    # Create json response of all Clarifai data, then write to csv
    resp_json = json_response_from_file()
    json_to_csv(resp_json, "test.csv")
