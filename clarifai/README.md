### Installation ###

Only package you need to install is clarifai
```
$ pip install clarifai
```

### Main Code ###

* `clarifai_main.py`: calls the Clarifai apis and saves the return to a json file for further processing.
* `load_response.py`: parses the json responses and returns a list of dictionaries, it can also save the response to csv format.

### Notes ###

* Due to the 5000 per month api calls, I chose to save the reponse in a file so we can view it and edit it later and pick the json format we want to deliver in. 
Be careful when running the script not to make too many api calls as we'd want to make the most out of the calls.

* Getting the response from the api can take up to ~30 seconds at times. not sure if this has to do with bandwidth, but I don;t think so, since I'm not uploading the image and the response is very small.

* The general model returns a total of 20 concepts.