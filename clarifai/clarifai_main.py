'''
    Author: Amr Awad <amro.khir@gmail.com>
    Date: 27-11-2017

    This script will use the clarifai api to perform the following operations:
    - Detect concepts in a page
    - Detect faces
    - Detect dominant colours

    For more details on Clarifai APIs, please refer to:
    https://clarifai.com/developer/guide/
'''

from clarifai.rest import ClarifaiApp
from clarifai.rest import Image as ClImage
import json
from load_response import *

# You can manage the api keys here: https://clarifai.com/developer/account/keys
appKey = "a088f5458df14629a1dd85695d6d0b41"
testUrl_1 = "http://api.slv.vic.gov.au/access_record/4737037"
testUrl_2 = "http://api.slv.vic.gov.au/access_record/2453209"
testUrl_3 = "http://api.slv.vic.gov.au/access_record/3275301"

testUrl = testUrl_3
app = ClarifaiApp(api_key=appKey)


def get_color_data(app, url_input):
    model = app.models.get('color')
    image = ClImage(url=url_input)
    response = model.predict([image])
    return response


def get_face_data(app, url_input):
    model = app.models.get('face-v1.3')
    image = ClImage(url=url_input)
    response = model.predict([image])
    return response


def get_object_data(app, url_input):
    model = app.models.get('general-v1.3')
    image = ClImage(url=url_input)
    response = model.predict([image])
    return response


def get_color_data_to_file(app, url_input):
    color_response = get_color_data(app, url_input)
    with open("color_response_3.json", "w") as f:
        f.write(str(color_response))


def get_face_data_to_file(app, url_input):
    face_response = get_face_data(app, url_input)
    with open("face_response_3.json", "w") as f:
        f.write(str(face_response))


def get_object_data_to_file(app, url_input):
    object_response = get_object_data(app, url_input)
    with open("object_response_3.json", "w") as f:
        f.write(str(object_response))


if __name__ == '__main__':
    '''
    get_color_data_to_file(app, testUrl)
    get_face_data_to_file(app, testUrl)
    get_object_data_to_file(app, testUrl)
    '''
    print("Invoking Clarifai Color Model ...")
    color_response = get_color_data(app, testUrl)
    print("Clarifai Color Concepts recieved ...")
    print("Invoking Clarifai Face Detection Model ...")
    face_response = get_face_data(app, testUrl)
    print("Clarifai Face Detections recieved ...")
    print("Invoking Clarifai Object Detection Model ...")
    object_response = get_object_data(app, testUrl)
    print("Clarifai Object Detections recieved ...")

    print("Processing Clarifai Color Response ...")
    colorDetails = process_color_response(color_response)
    print("Clarifai Color Response Processing Done")
    print("Processing Clarifai Face Response ...")
    faceDetails = process_face_response(face_response)
    print("Clarifai Face Response Processing Done")
    print("Processing Clarifai Object Response ...")
    objectDetails = process_object_response(object_response)
    print("Clarifai Object Response Processing Done")

    print('Creating json response ...')
    resp_json = json_response(colorDetails, faceDetails, objectDetails, testUrl)

    print('Outputing json response to csv')
    json_to_csv(resp_json, "test.csv")
    print('All Done :D')
